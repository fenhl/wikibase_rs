extern crate wikibase;

use wikibase::query;

/// Test if the URL is changed from the default (Wikidata) to another instance
///
/// Setting the environment variable should change the request URL that
/// is returned by a query.
#[test]
fn test_api_url() {
    let mut configuration = wikibase::Configuration::new("Testing/0.1").unwrap();
    let entity_query = query::EntityQuery::new(vec!["Q43453".to_string()], "uk");
    assert_eq!("https://www.wikidata.org/w/api.php?action=wbgetentities&ids=Q43453&languages=uk&uselang=uk&format=json",
    entity_query.url(&configuration));

    configuration.set_api_url("https://test.wikidata.org/wiki/api.php");
    assert_eq!("https://test.wikidata.org/wiki/api.php?action=wbgetentities&ids=Q43453&languages=uk&uselang=uk&format=json",
    entity_query.url(&configuration));
}

/// Test if user-agent is constructed correctly
///
/// User-agent has to consist of the user user-agent prefix, and the
/// Wikibase user-agent.
#[test]
fn test_user_agent() {
    let configuration = wikibase::Configuration::new("Testing/0.1").unwrap();
    assert_eq!("Testing/0.1 Wikibase-RS/0.1.5", configuration.user_agent());
}
