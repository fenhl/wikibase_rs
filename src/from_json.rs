// from_json.rs
//
// Copyright © 2018
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

extern crate serde_json;

use {
    Claim,
    Coordinate,
    DataValue,
    DataValueType,
    Entity,
    EntityType,
    EntityValue,
    LocaleString,
    LocaleStringArray,
    MonoLingualText,
    QuantityValue,
    Reference,
    SearchResults,
    SearchResultEntity,
    SiteLink,
    Snak,
    SnakType,
    TimeValue,
    Value,
    WikibaseError
};
use std;

/// Serializes a Wikibase entity from JSON
///
/// Takes a serde JSON value and serializes the complete data into a Rust
/// native data structure.
pub fn entity_from_json(json: &serde_json::Value, id: &str)
    -> Result<Entity, WikibaseError> {
    let mut entity = Entity::new_empty();
    let entities = &json["entities"];
    let item_object = &entities[id];

    match &item_object["id"].as_str() {
        &Some(value) => entity.set_id(value.to_string()),
        &None => return Err(WikibaseError::Serialization("ID missing".to_string()))
    };

    match &item_object.get("missing") {
        &Some(_) => {
            entity.set_missing(true);
            return Ok(entity)
        }
        &None => {}
    };

    entity.set_descriptions(locale_strings_from_json(&item_object, "descriptions")?);
    entity.set_aliases(locale_string_array_from_json(&item_object, "aliases")?);
    entity.set_labels(locale_strings_from_json(&item_object, "labels")?);
    entity.set_claims(claims_from_json(&item_object)?);
    entity.set_sitelinks(sitelinks_from_json(&item_object)?);
    Ok(entity)
}

/// Search result entity from JSON
///
/// Takes a serde JSON value of search results and serializes it. Returns a
/// vector of results or an error.
pub fn search_result_entities_from_json(json: &serde_json::Value)
    -> Result<SearchResults, WikibaseError> {
    let results_json = match &json["search"].as_array() {
        &Some(value) => value,
        &None => return Err(WikibaseError::Serialization("No search results".to_string()))
    };
    let mut results = vec![];

    for result in results_json {
        results.push(search_result_entity_from_json(&result)?);
    }

    Ok(SearchResults::new(results))
}

/// Creates a search result entity from JSON
///
/// The locale of the texts is the same as the `uselang` parameter in the
/// request URL.
fn search_result_entity_from_json(result_json: &serde_json::Value)
    -> Result<SearchResultEntity, WikibaseError> {
    let id = match result_json["id"].as_str() {
        Some(value) => value,
        None => return Err(WikibaseError::Serialization("Search result ID".to_string()))
    };
    let label_text = match result_json["label"].as_str() {
        Some(value) => value,
        None => return Err(WikibaseError::Serialization("Search result label".to_string()))
    };
    let label = LocaleString::new("en", label_text);
    let description = match result_json["description"].as_str() {
        Some(value) => Some(LocaleString::new("en", value)),
        None => None
    };
    let aliases = aliases_from_json(result_json)?;
    let entity_type = EntityType::new_from_id(id)?;

    Ok(SearchResultEntity::new(id, entity_type, label, description, aliases))
}

/// Alias from JSON
///
/// Takes a serde JSON value and serializes the aliases into a LocaleStringArray.
fn aliases_from_json(json_value: &serde_json::Value)
    -> Result<Option<LocaleStringArray>, WikibaseError> {
    let aliases_array = match json_value.get("aliases") {
        Some(value) => {
            match value.as_array() {
                Some(value_array) => value_array,
                None => return Err(WikibaseError::Serialization("Aliases array".to_string()))
            }
        },
        None => return Ok(None)
    };

    let mut alias_values = vec![];

    for alias in aliases_array.iter() {
        match alias.as_str() {
            Some(value) => alias_values.push(value.to_string()),
            None => return Err(WikibaseError::Serialization("Aliases string".to_string()))
        };
    }

    Ok(Some(LocaleStringArray::new("en", alias_values)))
}

/// Serializes a numeric value to a f64 option
///
/// Takes a value of the following structure:
/// {"key": "4"}
pub fn float_from_json(value: &serde_json::Map<std::string::String,
    serde_json::Value>, key: &str) -> Option<f64> {
    let single_value = match value.get(key) {
        Some(value) => value,
        None => return None
    };

    let value_string = match single_value.as_str() {
        Some(value) => value,
        None => return None,
    };

    match value_string.parse() {
        Ok(value) => Some(value),
        Err(_) => None
    }
}

/// Serializes a Wikidata claim
///
/// Takes a json object and returns a result.
///
/// # Errors
///
/// An error is returned when the json can't be interpreted as an object,
/// or the main snak is missing.
pub fn claim_from_json(json_claim: &serde_json::Value)
    -> Result<Claim, WikibaseError> {
    let mut claim = Claim::new();
    let claim_object = match &json_claim.as_object() {
        &Some(value) => value,
        &None => return Err(WikibaseError::Serialization("Claim".to_string()))
    };

    match claim_object["type"].as_str() {
        Some(value) => claim.set_claim_type(value),
        None => return Err(WikibaseError::Serialization("Claim type error".to_string()))
    };

    match claim_object["rank"].as_str() {
        Some(value) => claim.set_rank(value),
        None => return Err(WikibaseError::Serialization("Claim rank error".to_string()))
    };

    let main_snak = match &claim_object["mainsnak"].as_object() {
        &Some(snak_json) => {
            snak_from_json(snak_json)?
        }
        &None => return Err(WikibaseError::Serialization("Main snak".to_string()))
    };

    claim.set_main_snak(main_snak);

    match &claim_object.get("qualifiers") {
        // Qualifiers are an object with property keys that contain
        // an array of snaks. {"P817": [], ...}
        &Some(qualifiers) => {
            claim.set_qualifier_snaks(snaks_object_from_json(&qualifiers)?);
        }
        &None => {}
    };

    match &claim_object.get("references") {
        &Some(references_array_json) => {
            claim.set_references(reference_array_from_json(&references_array_json)?);
        }
        &None => {}
    };

    Ok(claim)
}

/// Serializes a locale string
///
/// A locale string is a JSON object:
/// {"language": "de", "value": "Label"}
///
/// Returns a LocaleString inside of an option:
/// LocaleString {language: "de", value: "Label"}
fn locale_string_from_json(string_object: &serde_json::Value)
    -> Result<LocaleString, WikibaseError> {
    let language = match string_object["language"].as_str() {
        Some(value) => value,
        None => return Err(WikibaseError::Serialization("Can't serialize language in locale string".to_string()))
    };

    match string_object["value"].as_str() {
        Some(value) => Ok(LocaleString::new(language, value)),
        None => Err(WikibaseError::Serialization("Can't serialize value in locale string".to_string()))
    }
}

/// Serializes an array of locale strings (e.g. aliases)
///
/// The JSON looks like this:
///
/// {"aliases": {"en", [{"language": "en", "value": "Alias 1"},
/// {"language": "en", "value": "Alias 2"}, ... ]}}
fn locale_string_array_from_json(item: &serde_json::Value, key: &str)
    -> Result<Vec<LocaleStringArray>, WikibaseError> {
    let mut aliases = vec![];

    let lang_object = match &item[key].as_object() {
        &Some(value) => value,
        &None => return Err(WikibaseError::Serialization("Locale object".to_string()))
    };

    for (language, string_array_json) in lang_object.iter() {
        let string_array = match string_array_json.as_array() {
            Some(value) => value,
            None => return Err(WikibaseError::Serialization("Locale array".to_string()))
        };

        let mut values = vec![];

        for item_json in string_array.iter() {
            match item_json["value"].as_str() {
                Some(value) => values.push(value.to_string()),
                None => return Err(WikibaseError::Serialization("Locale value".to_string()))
            }
        }

        aliases.push(LocaleStringArray::new(language.to_string(), values));
    }

    Ok(aliases)
}

/// Serialize single locale strings from JSON
///
/// Takes a serde-JSON value and returns a vector of locale strings inside
/// of a result.
fn locale_strings_from_json(item: &serde_json::Value, key: &str)
    -> Result<Vec<LocaleString>, WikibaseError> {
    let mut labels = vec![];

    let label_object = match &item.get(key) {
        &Some(value) => {
            match value.as_object() {
                Some(object) => object,
                None => return Err(WikibaseError::Serialization("Locale string object not valid".to_string()))
            }
        }
        &None => return Err(WikibaseError::Serialization(format!("Key \"{}\" not found in object", key)))
    };

    for (_, string_object) in label_object.iter() {
        labels.push(locale_string_from_json(string_object)?);
    }

    Ok(labels)
}

/// Serialize claims from JSON
///
/// Function that takes a json value (an object) and serializes all claims.
/// The claims are stored under the "claims" key. The first array has
/// property IDs as keys, with all claims for that property as values.
fn claims_from_json(item: &serde_json::Value)
    -> Result<Vec<Claim>, WikibaseError> {
    let claims_array = match &item["claims"].as_object() {
        &Some(value) => value,
        &None => return Err(WikibaseError::Serialization("Key \"claims\" missing in json object".to_string()))
    };

    let mut claims = vec![];

    for (_, property_claims) in claims_array.iter() {
        let property_claims_array = match &property_claims.as_array() {
            &Some(value) => value,
            &None => return Err(WikibaseError::Serialization("Claim array".to_string()))
        };

        for property_claim in property_claims_array.iter() {
            claims.push(claim_from_json(&property_claim)?);
        }
    }

    Ok(claims)
}

/// Serializes a Wikibase Snake from JSON
///
/// Takes a serde-JSON map and returns a single Snak inside or an error
/// inside of a result.
fn snak_from_json(snak_json: &serde_json::Map<std::string::String,
    serde_json::Value>) -> Result<Snak, WikibaseError> {
    let datatype = match snak_json["datatype"].as_str() {
        Some(value) => value,
        None => return Err(WikibaseError::Serialization("Datatype".to_string()))
    };
    let property = snak_json["property"].as_str().unwrap_or_else(|| {""});
    let snak_type = snak_json["snaktype"].as_str().unwrap_or_else(|| {""});

    if snak_type == "somevalue" {
        let snak = Snak::new(datatype, property, SnakType::UnknownValue, None);
        return Ok(snak)
    }

    if snak_type == "novalue" {
        let snak = Snak::new(datatype, property, SnakType::NoValue, None);
        return Ok(snak)
    }

    let data_value = match snak_json["datavalue"].as_object() {
        Some(value) => value,
        None => return Err(WikibaseError::Serialization("Data value not found".to_string()))
    };

    let data_value_type = match data_value["type"].as_str() {
        Some(value) => {
            DataValueType::new_from_str(value)?
        }
        None => return Err(WikibaseError::Serialization("Not type in data value".to_string()))
    };

    match data_value_type {
        DataValueType::EntityId => {
            let value_object = match data_value["value"].as_object() {
                Some(value) => value,
                None => return Err(WikibaseError::Serialization("Error serializing value object".to_string()))
            };
            let entity_value = EntityValue::new_from_object(value_object)?;
            let statement_value = Value::Entity(entity_value);
            let data_value = DataValue::new(data_value_type, statement_value);
            Ok(Snak::new(datatype, property, SnakType::Value, Some(data_value)))
        }
        DataValueType::Quantity => {
            let value_object = match data_value["value"].as_object() {
                Some(value) => value,
                None => return Err(WikibaseError::Serialization("Error serializing value object".to_string()))
            };
            let quantity_value = QuantityValue::new_from_object(value_object)?;
            let statement_value = Value::Quantity(quantity_value);
            let data_value = DataValue::new(data_value_type, statement_value);
            Ok(Snak::new(datatype, property, SnakType::Value, Some(data_value)))
        }
        DataValueType::GlobeCoordinate => {
            let value_object = match data_value["value"].as_object() {
                Some(value) => value,
                None => return Err(WikibaseError::Serialization("Error serializing value object".to_string()))
            };
            let coordinate = Coordinate::new_from_json(value_object)?;
            let coordinate_statement = Value::Coordinate(coordinate);
            let data_value = DataValue::new(data_value_type, coordinate_statement);
            Ok(Snak::new(datatype, property, SnakType::Value, Some(data_value)))
        }
        DataValueType::Time => {
            let value_object = match data_value["value"].as_object() {
                Some(value) => value,
                None => return Err(WikibaseError::Serialization("Error serializing value object".to_string()))
            };
            let time_value_struct = TimeValue::new_from_object(value_object);
            let time_value = Value::Time(time_value_struct);
            let data_value = DataValue::new(data_value_type, time_value);
            Ok(Snak::new(datatype, property, SnakType::Value, Some(data_value)))
        }
        DataValueType::MonoLingualText => {
            let value_object = match data_value["value"].as_object() {
                Some(value) => value,
                None => return Err(WikibaseError::Serialization("Error serializing value object".to_string()))
            };
            let mono_lingual_text = MonoLingualText::new_from_json(value_object)?;
            let mono_lingual_text_value = Value::MonoLingual(mono_lingual_text);
            let data_value = DataValue::new(data_value_type, mono_lingual_text_value);
            Ok(Snak::new(datatype, property, SnakType::Value, Some(data_value)))
        }
        DataValueType::StringType => {
            let value_string = match data_value["value"].as_str() {
                Some(value) => value,
                None => return Err(WikibaseError::Serialization("Error serializing value string".to_string()))
            };
            let statement_value = Value::StringValue(value_string.to_string());
            let data_value = DataValue::new(data_value_type, statement_value);
            Ok(Snak::new(datatype, property, SnakType::Value, Some(data_value)))
        }
    }
}

/// Serializes an object of snaks from JSON
///
/// Wikibase uses this structure for qualfiers and the references.
///
/// # JSON Documentation
///
/// Serializes a json that looks like this:
///
/// `
/// {"qualifiers": {"P1": [Snak1, Snak2], ... ]}}
/// `
fn snaks_object_from_json(snaks_object_json: &serde_json::Value)
    -> Result<Vec<Snak>, WikibaseError> {
    let mut snaks = vec![];

    let snaks_object = match &snaks_object_json.as_object() {
        &Some(value) => value,
        &None => return Err(WikibaseError::Serialization("Can't serialize snaks object".to_string()))
    };

    // Loop over the property keys e.g. {"P817": [], ...}
    for (_, property_snaks) in snaks_object.iter() {
        let snak_array = match &property_snaks.as_array() {
            &Some(value) => value,
            &None => return Err(WikibaseError::Serialization("Can't serialize snak array".to_string()))
        };

        // Loop over the qualifiers of one property e.g. {"P817": [Snak-A, Snak-B, ...]}
        for snak_json in snak_array.iter() {
            let snak_object = match &snak_json.as_object() {
                &Some(value) => value,
                &None => return Err(WikibaseError::Serialization("Can't serialize snak object".to_string()))
            };

            snaks.push(snak_from_json(snak_object)?);
        }
    }

    Ok(snaks)
}

/// Serializes the references of a claim
///
/// Takes a serde-JSON value an returns a vector of references or an error
/// inside of a result. The JSON looks like this:
/// {"references": ["0": {"snaks": {"P1": [Snak1, Snak2]}, ... }]}
fn reference_array_from_json(references_array_json: &serde_json::Value)
    -> Result<Vec<Reference>, WikibaseError> {
    let mut references = vec![];

    let reference_array = match &references_array_json.as_array() {
        &Some(value) => value,
        &None => return Err(WikibaseError::Serialization("Reference array".to_string()))
    };

    for references_object_json in reference_array.iter() {
        let reference_object = match &references_object_json.as_object() {
            &Some(value) => value,
            &None => return Err(WikibaseError::Serialization("Reference object".to_string()))
        };

        let snaks_array = match &reference_object.get("snaks") {
            &Some(value) => value,
            &None => return Err(WikibaseError::Serialization("Reference snak object".to_string()))
        };

        references.push(Reference::new(snaks_object_from_json(snaks_array)?));
    }

    Ok(references)
}

/// Serialize a vector of strings from JSON
///
/// Takes a serde-JSON value and returns a vector of Strings or an error
/// inside of a Result.
fn string_vector_from_json(json_values: &Vec<serde_json::Value>)
    -> Result<Vec<String>, WikibaseError> {
    let mut values = vec![];

    for json_value in json_values {
        let value = match json_value.as_str() {
            Some(value) => value,
            None => return Err(WikibaseError::Serialization("Error serializing string vector".to_string()))
        };

        values.push(value.to_string());
    }

    Ok(values)
}

/// Serialize sitelinks from JSON
///
/// Takes a serde-JSON value and returns a Result that contains either a
/// error or a Option. Properties for example don't contain sitelinks, so
/// the option will be None. If a Wikibase-item has sitelinks the Option
/// will contain a vector of Sitelinks.
fn sitelinks_from_json(item: &serde_json::Value)
    -> Result<Option<Vec<SiteLink>>, WikibaseError> {
    let sitelinks_array = match &item.get("sitelinks") {
        &Some(value) => {
            match value.as_object() {
                Some(value_object) => value_object,
                None => return Err(WikibaseError::Serialization("Key \"sitelinks\" missing in json object".to_string()))
            }
        }
        &None => return Ok(None)
    };

    let mut sitelinks = vec![];

    for (site_id, sitelink_object_json) in sitelinks_array.iter() {
        let sitelink_object = match &sitelink_object_json.as_object() {
            &Some(value) => value,
            &None => return Err(WikibaseError::Serialization("Error serializing sitelink object".to_string()))
        };

        let title = match &sitelink_object["title"].as_str() {
            &Some(value) => value,
            &None => return Err(WikibaseError::Serialization("Error serializing sitelink title".to_string()))
        };

        let badges = match &sitelink_object["badges"].as_array() {
            &Some(value) => {
                string_vector_from_json(value)?
            }
            &None => return Err(WikibaseError::Serialization("Error serializing badges array".to_string()))
        };

        let sitelink = SiteLink::new(site_id.to_string(), title.to_string(), badges);
        sitelinks.push(sitelink);
    }

    Ok(Some(sitelinks))
}
