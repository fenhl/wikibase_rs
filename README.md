# Wikibase RS

A tool to communicate with Wikibase (the most famous instance being Wikidata).
Currently only reading the JSON-API is possible. Parsing the JSON-dumps and
editing Wikibase are on the roadmap.

## Installation

```bash
cargo install wikibase
```

## Usage

See documentation linked from crates.io

## Contributing

This tool and the community around it are part of the Wikimedia movement.
Everybody is welcome to contribute. Plase refer to this guide
https://www.mediawiki.org/wiki/Code_of_Conduct
before contributing.

## Licence

GNU General Public License, Version 2 or later (GPL-2.0+)

## Repository / Issue tracker

https://gitlab.com/tobias47n9e/wikibase_rs

## Crates.io

https://crates.io/crates/wikibase

## Free software using Wikibase RS

* WKDR (https://crates.io/crates/wkdr)

## Custom Wikibase instance

By default Wikibase RS will use Wikidata. To use a different Wikibase
instance you have to change the default configuration:

```rust
let configuration = wikibase::Configuration::new("My-Fancy-App/1.0").unwrap();
configuration.set_api_url("https://www.my-wikibase-instance.rs/w/api.php");
```

## Dedication

Dedicated to Krzysztof Machocki. We will remember Krzysztof with his
contagious enthusiasm and dedication, for his vast encyclopedic knowledge
and wit, for his faith in how Wikipedia makes the world better, both online
and offline. We lose him but let us not lose the faith he shared with the
world. The memory of him remains and his Wikipedia legacy will continue
to help the world.

https://meta.wikimedia.org/wiki/CEE/Newsletter/January_2018/Contents/From_the_team
